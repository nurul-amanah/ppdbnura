<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::resource('santri', 'SantriController');
    Route::get('/verifikasi/{id}', 'SantriController@verifikasi')->name('backend.verifikasi');
    Route::put('/verifikasi/{id}', 'SantriController@updateVerifikasi')->name('santri.verifikasi');
});
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/pendaftaran-santri-baru', 'SantriController@create')->name('santri.create');
Route::post('/pendaftaran-santri-baru', 'SantriController@store')->name('santri.store');
Route::get('/detail-pendaftaran', 'SantriController@detailPendaftaran')->name('santri.detail-pendaftaran');
Route::get('/', 'SantriController@landing')->name('ppdb.index');
Route::get('/cek-data', 'SantriController@cek')->name('frontend.cek');
Route::post('/cek-data', 'SantriController@cekData')->name('frontend.cek-data');
Route::get('/alur-verifikasi', 'SantriController@alurVerifikasi')->name('frontend.alur-verifikasi');

Auth::routes([
    'register' => false
]);
