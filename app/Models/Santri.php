<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Santri extends Model
{
    protected $table = 'santris';

    public function ibu()
    {
        return $this->hasOne(Ibu::class);
    }

    public function ayah()
    {
        return $this->hasOne(Ayah::class);
    }

    public function persyaratan()
    {
        return $this->hasOne(Persyaratan::class);
    }

    public function verifikasi()
    {
        return $this->hasOne(Verifikasi::class);
    }
}
