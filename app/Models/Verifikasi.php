<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Verifikasi extends Model
{
    protected $table = 'verifikasis';

    protected $guarded = [];
    public function santri()
    {
        return $this->belongsTo(Santri::class);
    }
}
