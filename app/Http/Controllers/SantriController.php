<?php

namespace App\Http\Controllers;

use App\Models\Ayah;
use App\Models\Ibu;
use App\Models\Santri;
use App\Models\Verifikasi;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Str;

class SantriController extends Controller
{
    protected $rules = [
        'nama' => 'required|min:5|max:100',
        'panggilan' => 'required|max:20',
        'jeniskelamin' => 'required',
        'nisn'   => 'unique:santris',
        'tempatlahir' => 'required',
        'tanggallahir' => 'required',
        'anakke'  => 'required',
        'saudara'   => 'required',
        'tinggalbersama' => 'required',
        'statuskeluarga' => 'required',
        'alamat'    => 'required|min:5',
        'jenjangpendidikan'   => 'required',
        'statussiswa' => 'required',
        'asalsekolah' => 'required|min:5',
        'alamatsekolah' => 'required',
        // rules validation ayah
        'namaayah' => 'required',
        'agamaayah' => 'required',
        'pendidikanayah' => 'required',
        'pekerjaanayah' => 'required',
        'alamatayah' => 'required',
        // rules validation ibu
        'namaibu' => 'required',
        'agamaibu' => 'required',
        'pendidikanibu' => 'required',
        'pekerjaanibu' => 'required',
        'penghasilan' => 'required',
        'nowali'    => 'required',
        'foto_diri' => 'max:2048'
    ];

    protected $messages = [
        'required' => 'Kolom wajib diisi',
        'min' => 'Kolom setidaknya berisi :min karakter',
        'max' => 'Kolom maksimal berisi :max karakter',
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $jenjang =  $request->get('status');
        if ($request->get('status') == 'mts') {
            $santris = Santri::where('jenjang_pendidikan', 'MTs Nurul Amanah')->where('validasi', 'Belum')->orderBy('created_at', 'DESC')->get();
        } else if ($request->get('status') == 'santri') {
            $santris = Santri::where('pondok', $jenjang)->where('validasi', 'Belum')->orderBy('created_at', 'DESC')->get();
        } else if ($request->get('status') == 'smp') {
            $santris = Santri::where('jenjang_pendidikan', 'SMP Nurul Amanah')->where('validasi', 'Belum')->orderBy('created_at', 'DESC')->get();
        } else if ($request->get('status') == 'sma') {
            $santris = Santri::where('jenjang_pendidikan', 'SMA Nurul Amanah')->where('validasi', 'Belum')->orderBy('created_at', 'DESC')->get();
        } else if ($request->get('status') == 'smk') {
            $santris = Santri::where('jenjang_pendidikan', 'SMK Nurul Amanah')->where('validasi', 'Belum')->orderBy('created_at', 'DESC')->get();
        } else {
            $santris = Santri::where('validasi', 'Belum')->orderBy('created_at', 'DESC')->get();
        }


        $jumlah_santri = [
            'mts' => count(Santri::where('jenjang_pendidikan', 'MTs Nurul Amanah')->get()),
            'smp' => count(Santri::where('jenjang_pendidikan', 'SMP Nurul Amanah')->get()),
            'sma' => count(Santri::where('jenjang_pendidikan', 'SMA Nurul Amanah')->get()),
            'smk' => count(Santri::where('jenjang_pendidikan', 'SMK Nurul Amanah')->get()),
        ];
        return view('backend.students.index')->withSantris($santris)->withCount($jumlah_santri);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frontend.pendaftaran');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules, $this->messages);
        if ($validator->fails()) {
            Alert::warning('Maaf', 'Silahkan perbaiki data yang bertanda merah');
            return redirect()->back()->withErrors($validator)->withInput();
        }
        try {
            DB::beginTransaction();
            $santri = new Santri;
            $santri->nama_santri = $request->nama;
            $santri->panggilan_santri = $request->panggilan;
            $santri->nisn = $request->nisn;
            $santri->npsn = $request->npsn;
            $santri->jenis_kelamin = $request->jeniskelamin;
            $santri->tempat_lahir = $request->tempatlahir;
            $santri->tanggal_lahir = $request->tanggallahir;
            $santri->anak_ke = $request->anakke;
            $santri->jumlah_saudara = $request->saudara;
            $santri->tinggal_bersama = $request->tinggalbersama;
            $santri->status_dalam_keluarga = $request->statuskeluarga;
            $santri->alamat = $request->alamat;
            $santri->jenjang_pendidikan = $request->jenjangpendidikan;
            $santri->jurusan = $request->Jurusan;
            $santri->status_siswa = $request->statussiswa;
            $santri->asal_sekolah = $request->asalsekolah;
            $santri->alamat_sekolah = $request->alamatsekolah;
            $santri->pondok = $request->pondok;
            $santri->validasi = 'Belum';
            $santri->rekomendasi = $request->rekomendasi;
            $santri->nama_teman = $request->nama_teman;
            if ($request->hasFile('foto_diri')) {
                $file = $request->file('foto_diri');
                $file_name = date('dmysh') . '-' .  Str::slug($request->nama, '-') . '.' . $file->getClientOriginalExtension();
                $file->move('avatar', $file_name);
                $santri->foto_diri = $file_name;
            }
            $santri->save();

            // get santri was saved
            $santriId = Santri::find($santri->id);

            // create object ayah
            $ayah = new Ayah();
            $ayah->nama_ayah = $request->namaayah;
            $ayah->agama_ayah = $request->agamaayah;
            $ayah->alamat_ayah = $request->alamatayah;
            $ayah->pendidikan_terakhir_ayah = $request->pendidikanayah;
            $ayah->pekerjaan_ayah = $request->pekerjaanayah;
            $ayah->penghasilan_ayah = $request->penghasilan;
            $ayah->nomor_hp = $request->nowali;
            $santriId->ayah()->save($ayah);

            // create object ibu
            $ibu = new Ibu();
            $ibu->nama_ibu = $request->namaibu;
            $ibu->agama_ibu = $request->agamaibu;
            $ibu->alamat_ibu = $request->alamatibu;
            $ibu->pendidikan_terakhir_ibu = $request->pendidikanibu;
            $ibu->pekerjaan_ibu = $request->pekerjaanibu;
            $santriId->ibu()->save($ibu);
            DB::commit();
            Alert::success('Selamat!', 'Selamat anda telah berhasil melakukan pendaftaran!');
            $request->session()->put('siswaBaru', $santri);
            $request->session()->put('ayahSiswa', $ayah);
            $request->session()->put('ibuSiswa', $ibu);
            return redirect('/detail-pendaftaran');
        } catch (\Throwable $th) {
            throw $th;
            Alert::warning('Oopps', 'Terjadi kesalahan saat memasukan data');
            return redirect()->back()->withErrors($th->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $santri = Santri::find($id);
        return view('backend.students.detail')->withSantri($santri);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        return view('backend.students.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $santri = Santri::find($id);
        $santri->delete();
        Alert::success('Selamat', 'Data Berhasil di Hapus');
        return redirect()->back();
    }

    public function detailPendaftaran(Request $request)
    {
        if ($request->session()->get('siswaBaru')) {
            return view('frontend.detail-pendaftaran');
        } else {
            return view('frontend.pendaftaran');
        }
    }

    public function landing()
    {
        return view('frontend.index');
    }

    public function verifikasi($id)
    {
        $santri = Santri::find($id);
        $verifikasi = Verifikasi::where('santri_id', $santri->id)->first();
        return view('backend.verifikasi')->withVerifikasi($verifikasi)->withSantri($santri);
    }

    public function updateVerifikasi(Request $request, $id)
    {
        $santri = Santri::find($id);
        if (Verifikasi::where('santri_id', $id)->get()->count() > 0) {
            Verifikasi::where('santri_id', $id)
                ->update([
                    'fc_ijazah' => $request->get('fc_ijazah')  == 'on' ? 'Sudah' : 'Belum',
                    'skl'   => $request->get('skl') == 'on' ? 'Sudah' : 'Belum',
                    'foto'  => $request->get('foto') == 'on' ? 'Sudah' : 'Belum',
                    'fc_kak'    => $request->get('fc_kk') == 'on' ? 'Sudah' : 'Belum',
                    'fc_akta' => $request->get('fc_akta') == 'on' ? 'Sudah' : 'Belum',
                    'fc_ktp_ortu'   => $request->get('fc_ktp_ortu') == 'on' ? 'Sudah' : 'Belum',
                    'fc_kip'    => $request->get('fc_kip') == 'on' ? 'Sudah' : 'Belum',
                    'santri_id' => $santri->id
                ]);

            return 'Berhasil diupdate';
        } else {
            Verifikasi::create([
                'fc_ijazah' => $request->get('fc_ijazah')  == 'on' ? 'Sudah' : 'Belum',
                'skl'   => $request->get('skl') == 'on' ? 'Sudah' : 'Belum',
                'foto'  => $request->get('foto') == 'on' ? 'Sudah' : 'Belum',
                'fc_kak'    => $request->get('fc_kk') == 'on' ? 'Sudah' : 'Belum',
                'fc_akta' => $request->get('fc_akta') == 'on' ? 'Sudah' : 'Belum',
                'fc_ktp_ortu'   => $request->get('fc_ktp_ortu') == 'on' ? 'Sudah' : 'Belum',
                'fc_kip'    => $request->get('fc_kip') == 'on' ? 'Sudah' : 'Belum',
                'santri_id' => $santri->id
            ]);

            return 'Berhasil di buat';
        }
    }

    public function cek()
    {
        return view('frontend.cek');
    }

    public function cekData(Request $request)
    {
        $santri = Santri::where('nisn', $request->nisn)
            ->orWhere('nama_santri', $request->nama)->first();
        return view('frontend.hasil-cek')->withSantri($santri);
    }

    public function alurVerifikasi()
    {
        return view('frontend.alur-verifikasi');
    }
}
