@extends('backend.layouts.app')

@section('content')

<!-- Start Content-->
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <form class="form-inline">
                        <div class="form-group">
                            <div class="input-group input-group-sm">

                                <input type="text" class="form-control border-white" id="dash-daterange">
                                <div class="input-group-append">
                                    <span class="input-group-text bg-blue border-blue text-white">
                                        <i class="mdi mdi-calendar-range font-13"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <a href="javascript: void(0);" class="btn btn-blue btn-sm ml-2">
                            <i class="mdi mdi-autorenew"></i>
                        </a>
                        <a href="javascript: void(0);" class="btn btn-blue btn-sm ml-1">
                            <i class="mdi mdi-filter-variant"></i>
                        </a>
                    </form>
                </div>
                <h4 class="page-title">Dashboard</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-md-6 col-xl-3">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-6">
                        <div class="avatar-lg rounded-circle bg-primary border-primary border shadow">
                            <i class="fas fa-school font-22 avatar-title text-white"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <h3 class="mt-1"><span data-plugin="counterup">{{ $count['mts'] }}</span></h3>
                            <p class="text-muted mb-1 text-truncate">Total MTs</p>
                        </div>
                    </div>
                </div> <!-- end row-->
            </div> <!-- end widget-rounded-circle-->
        </div> <!-- end col-->

        <div class="col-md-6 col-xl-3">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-6">
                        <div class="avatar-lg rounded-circle bg-success border-success border shadow">
                            <i class="fas fa-school font-22 avatar-title text-white"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <h3 class="text-dark mt-1"><span data-plugin="counterup">{{ $count['smp'] }}</span></h3>
                            <p class="text-muted mb-1 text-truncate">Total SMP</p>
                        </div>
                    </div>
                </div> <!-- end row-->
            </div> <!-- end widget-rounded-circle-->
        </div> <!-- end col-->

        <div class="col-md-6 col-xl-3">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-6">
                        <div class="avatar-lg rounded-circle bg-info border-info border shadow">
                            <i class="fas fa-school font-22 avatar-title text-white"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <h3 class="text-dark mt-1"><span data-plugin="counterup">{{ $count['sma'] }}</span></h3>
                            <p class="text-muted mb-1 text-truncate">Total SMA</p>
                        </div>
                    </div>
                </div> <!-- end row-->
            </div> <!-- end widget-rounded-circle-->
        </div> <!-- end col-->

        <div class="col-md-6 col-xl-3">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-6">
                        <div class="avatar-lg rounded-circle bg-warning border-warning border shadow">
                            <i class="fas fa-school font-22 avatar-title text-white"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <h3 class="text-dark mt-1"><span data-plugin="counterup">{{ $count['smk'] }}</span></h3>
                            <p class="text-muted mb-1 text-truncate">Total SMK</p>
                        </div>
                    </div>
                </div> <!-- end row-->
            </div> <!-- end widget-rounded-circle-->
        </div> <!-- end col-->
    </div>
    <div class="row">
        <!--<div class="col-md-12 col-xl-4">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-5">
                        <div class="avatar-lg rounded-circle shadow-sm bg-primary border-primary border">
                            <i class="fas fa-male font-22 avatar-title text-white"></i>
                        </div>
                    </div>
                    <div class="col-7">
                        <div class="text-right">
                            <h4 class="text-dark mt-1"><span data-plugin="counterup">{{ App\Models\Santri::where('validasi', 'Belum')->where('jenis_kelamin', 'laki-laki')->count() }}</span></h4>
                            <p class="text-muted mb-1 text-truncate">Santri Putra</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
        <!--<div class="col-md-12 col-xl-4">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-5">
                        <div class="avatar-lg rounded-circle shadow-sm bg-info border-info border">
                            <i class="fas fa-female font-22 avatar-title text-white"></i>
                        </div>
                    </div>
                    <div class="col-7">
                        <div class="text-right">
                            <h4 class="text-dark mt-1"><span data-plugin="counterup">{{ App\Models\Santri::where('validasi', 'Belum')->where('jenis_kelamin', 'perempuan')->count() }}</span></h4>
                            <p class="text-muted mb-1 text-truncate">Santri Putri</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
        <div class="col-md-12 col-xl-12">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-5">
                        <div class="avatar-lg rounded-circle shadow-sm bg-success border-info border">
                            <i class="fas fa-users font-22 avatar-title text-white"></i>
                        </div>
                    </div>
                    <div class="col-7">
                        <div class="text-right">
                            <h4 class="text-dark mt-1"><span data-plugin="counterup">{{ App\Models\Santri::where('validasi', 'Belum')->count() }}</span></h4>
                            <p class="text-muted mb-1 text-truncate">Total Santri</p>
                        </div>
                    </div>
                </div> <!-- end row-->
            </div> <!-- end widget-rounded-circle-->
        </div>
    </div>


    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-success text-white">
                    Data Seluruh Santri
                </div>
                <div class="card-body">
                    <table id="basic-datatable" class="table responsive nowrap table-bordered table-striped table-sm">
                        <thead>
                            <tr>
                                <th scope="row">NO</th>
                                <th>NISN</th>
                                <th>Nama Lengkap</th>
                                <th>Status Pondok</th>
                                <th>Jenjang Pendidikan</th>
                                <th>Validasi</th>
                                <th>Option</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($santris as $key => $santri)
                            <tr>
                                <td class="align-middle">{{ $key+1 }}</td>
                                <td class="align-middle">{{ $santri->nisn }}</td>
                                <td class="align-middle">{{ $santri->nama_santri }}</td>
                                <td class="align-middle">
                                    @if ($santri->pondok == 'Pondok')
                                        <div class="badge bg-soft-success font-13 p-1  text-success">{{ $santri->pondok }}</div>
                                    @else
                                        <div class="badge bg-soft-warning font-13 p-1 text-warning">{{ $santri->pondok }}</div>
                                    @endif
                                </td>
                                <td class="align-middle">
                                    {{ $santri->jenjang_pendidikan }}
                                </td>
                                <td class="align-middle">
                                    <div class="badge bg-soft-danger font-13 p-1  text-danger">{{ $santri->validasi }}</div>
                                </td>
                                <td class="align-middle text-center">
                                    <div class="row justify-content-center">
                                        <a class="bg-soft-info m-1 text-info btn-sm shadow-none" href="{{ route('backend.verifikasi', $santri->id) }}">
                                            <i class="fa fa-pen-square"></i>
                                        </a>
                                        <a class="bg-soft-success m-1 text-success btn-sm shadow-none" href="{{ route('santri.show', $santri->id) }}">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <form action="{{ route('santri.destroy', $santri->id) }}" method="post" onsubmit="return confirm('Yakin untuk menghapus data ini?')">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-sm bg-soft-danger m-1 text-danger btn-sm shadow-none" >
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->
</div>

@endsection

@section('css')
    <!-- third party css -->
    <link href="{{ url('/') }}/backend/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/backend/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/backend/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/backend/assets/libs/datatables/select.bootstrap4.css" rel="stylesheet" type="text/css" />
    <!-- third party css end -->
@endsection

@section('js')
    <!-- third party js -->
    <script src="{{ url('/') }}/backend/assets/libs/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ url('/') }}/backend/assets/libs/datatables/dataTables.bootstrap4.js"></script>
    <script src="{{ url('/') }}/backend/assets/libs/datatables/dataTables.responsive.min.js"></script>
    <script src="{{ url('/') }}/backend/assets/libs/datatables/responsive.bootstrap4.min.js"></script>
    <script src="{{ url('/') }}/backend/assets/libs/datatables/dataTables.buttons.min.js"></script>
    <script src="{{ url('/') }}/backend/assets/libs/datatables/buttons.bootstrap4.min.js"></script>
    <script src="{{ url('/') }}/backend/assets/libs/datatables/buttons.html5.min.js"></script>
    <script src="{{ url('/') }}/backend/assets/libs/datatables/buttons.flash.min.js"></script>
    <script src="{{ url('/') }}/backend/assets/libs/datatables/buttons.print.min.js"></script>
    <script src="{{ url('/') }}/backend/assets/libs/datatables/dataTables.keyTable.min.js"></script>
    <script src="{{ url('/') }}/backend/assets/libs/datatables/dataTables.select.min.js"></script>
    <script src="{{ url('/') }}/backend/assets/libs/pdfmake/pdfmake.min.js"></script>
    <script src="{{ url('/') }}/backend/assets/libs/pdfmake/vfs_fonts.js"></script>
    <!-- third party js ends -->

    <!-- Datatables init -->
    <script src="{{ url('/') }}/backend/assets/js/pages/datatables.init.js"></script>
@endsection
