@extends('backend.layouts.app')

@section('content')
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Santri</a></li>
                            <!--<li class="breadcrumb-item"><a href="javascript: void(0);">Extras</a></li>-->
                            <li class="breadcrumb-item active">Profile Santri</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Profile Santri <span class="text-blue"></span></span></h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Custom Modal -->
        <div id="modal-foto" class="modal-sm modal-demo">
            <button type="button" class="close" onclick="Custombox.modal.close();">
                <span>&times;</span><span class="sr-only">Close</span>
            </button>
            <h4 class="custom-modal-title">Ubah Foto</h4>
            <div class="custom-modal-text">
                <form>
                    <input type='file' id="user_group_logo" class="custom-file-input" name="foto">
                    <div class="text-center">
                        <label id="user_group_label" for="user_group_logo"><i class="fas fa-upload"></i>&nbsp;Choose an image...</label>
                    </div><br>
                    <div class="text-center">
                        <button type="submit" name="kirim" class="btn btn-success">Kirim</button>
                        <button type="reset" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-xl-4">
                <div class="card-box text-center">
                    <div class="dropdown">
                        <a class="nav-link dropdown-toggle h5 mt-2 mb-1 d-block" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <img src="{{ asset('backend/assets/images/users/user-1.jpg') }}" class="rounded-circle img-thumbnail"
                            alt="profile-image">
                        </a>
                        <div class="dropdown-menu user-pro-dropdown">
                            <a href="#modal-foto" class="dropdown-item notify-item" data-animation="fadein" data-plugin="custommodal">
                                <i class="fa fa-pen"></i>
                                <span>Ubah Foto</span>
                            </a>
                        </div>
                    </div>

                    <h4 class="mb-0">{{ $santri->nama_santri }}</h4>
                    <p class="text-blue">{{ $santri->nisn }}</p></p>

                    <div class="text-left mt-3">
                        <h4 class="font-13 text-uppercase">Biodata :</h4>
                        <table class="table table-sm table-borderless table-responsive">
                            <tr>
                                <th>NISN</th>
                                <td>:</td>
                                <td>{{ $santri->nisn }}</td>
                            </tr>
                            <tr>
                                <th>Nama Santri</th>
                                <td>:</td>
                                <td>{{ $santri->nama_santri }}</td>
                            </tr>
                            <tr>
                                <th>Nomor Tlp Orang Tua</th>
                                <td>:</td>
                                <td>{{ $santri->ayah->nomor_hp }}</td>
                            </tr>
                        </table>
                    </div>
                </div> <!-- end card-box -->

            </div> <!-- end col-->

            <div class="col-lg-8 col-xl-8">
                <div class="card-box">
                    <ul class="nav nav-pills navtab-bg nav-justified">
                        <li class="nav-item">
                            <a href="#santri" data-toggle="tab" aria-expanded="true" class="nav-link active">
                                Biodata Santri
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#wali" data-toggle="tab" aria-expanded="false" class="nav-link ">
                                Biodata Wali Santri
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#other" data-toggle="tab" aria-expanded="false" class="nav-link">
                                Other
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane show active" id="santri">
                            <table class="table table-sm table-borderless ">
                                <tr>
                                    <th>NISN</th>
                                    <td>:</td>
                                    <td>{{ $santri->nisn }}</td>
                                </tr>
                                <tr>
                                    <th>NIK</th>
                                    <td>:</td>
                                    <td>{{ $santri->npsn }}</td>
                                </tr>
                                <tr>
                                    <th>Nama Lengkap</th>
                                    <td>:</td>
                                    <td>{{ $santri->nama_santri }}</td>
                                </tr>
                                <tr>
                                    <th>Nama Panggilan</th>
                                    <td>:</td>
                                    <td>{{ $santri->panggilan_santri }}</td>
                                </tr>
                                <tr>
                                    <th>Tempat Lahir</th>
                                    <td>:</td>
                                    <td>{{ $santri->tempat_lahir }}</td>
                                </tr>
                                <tr>
                                    <th>Tanggal Lahir</th>
                                    <td>:</td>
                                    <td>{{ $santri->tanggal_lahir }}</td>
                                </tr>
                                <tr>
                                    <th>Anak Ke</th>
                                    <td>:</td>
                                    <td>{{ $santri->anak_ke }}</td>
                                </tr>
                                <tr>
                                    <th>Jumlah Saudara</th>
                                    <td>:</td>
                                    <td>{{ $santri->jumlah_saudara }}</td>
                                </tr>
                                <tr>
                                    <th>Status Dalam Keluarga</th>
                                    <td>:</td>
                                    <td>{{ $santri->status_dalam_keluarga }}</td>
                                </tr>
                                <tr>
                                    <th>Alamat</th>
                                    <td>:</td>
                                    <td>{{ $santri->alamat }}</td>
                                </tr>
                                <tr>
                                    <th>Masuk Jenjang Pendidikan</th>
                                    <td>:</td>
                                    <td>{{ $santri->jenjang_pendidikan }}</td>
                                </tr>
                                <tr>
                                    <th>Jurusan</th>
                                    <td>:</td>
                                    <td>{{ $santri->jurusan ? $santri->jurusan : '-' }}</td>
                                </tr>
                                <tr>
                                    <th>Satus Siswa</th>
                                    <td>:</td>
                                    <td>{{ $santri->status_santri }}</td>
                                </tr>
                                <tr>
                                    <th>Asal Sekolah</th>
                                    <td>:</td>
                                    <td>{{ $santri->asal_sekolah }}</td>
                                </tr>
                                <tr>
                                    <th>Alamat Sekolah</th>
                                    <td>:</td>
                                    <td>{{ $santri->alamat_sekolah }}</td>
                                </tr>
                                <tr>
                                    <th>Rekomendasi</th>
                                    <td>:</td>
                                    <td>{{ $santri->rekomendasi }}</td>
                                </tr>
                            </table>

                        </div> <!-- end tab-pane -->
                        <!-- end about me section content -->

                        <div class="tab-pane " id="wali">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            Data Ayah
                                        </div>
                                        <div class="card-body">
                                            <table class="table table-sm table-borderless">
                                                <tr>
                                                    <th>Nama Ayah</th>
                                                    <td>:</td>
                                                    <td>{{ $santri->ayah->nama_ayah }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Agama Ayah</th>
                                                    <td>:</td>
                                                    <td>{{ $santri->ayah->agama_ayah }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Pendidikan Trakhir Ayah</th>
                                                    <td>:</td>
                                                    <td>{{ $santri->ayah->pendidikan_terakhir_ayah }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Pekerjaan Ayah</th>
                                                    <td>:</td>
                                                    <td>{{ $santri->ayah->pekerjaan_ayah }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Alamat Ayah</th>
                                                    <td>:</td>
                                                    <td>{{ $santri->ayah->alamat_ayah }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Penghasilan Orang Tua Perbulan</th>
                                                    <td>:</td>
                                                    <td><b>Rp. {{ $santri->ayah->penghasilan_ayah }}</b></td>
                                                </tr>
                                                <tr>
                                                    <th>Nomor HP/WA Orang Tua/Wali</th>
                                                    <td>:</td>
                                                    <td>{{ $santri->ayah->nomor_hp }}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            Data Ibu
                                        </div>
                                        <div class="card-body">
                                            <table class="table table-sm table-borderless">
                                                <tr>
                                                    <th>Nama Ibu</th>
                                                    <td>:</td>
                                                    <td>{{ $santri->ibu->nama_ibu }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Agama Ibu</th>
                                                    <td>:</td>
                                                    <td>{{ $santri->ibu->agama_ibu }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Pendidikan Trakhir Ibu</th>
                                                    <td>:</td>
                                                    <td>{{ $santri->ibu->pendidikan_terakhir_ibu }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Pekerjaan Ibu</th>
                                                    <td>:</td>
                                                    <td>{{ $santri->ibu->pekerjaan_ibu }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Alamat Ibu</th>
                                                    <td>:</td>
                                                    <td>{{ $santri->ibu->alamat_ibu }}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end wali content-->

                        <div class="tab-pane" id="other">

                        </div>
                        <!-- end other content-->

                    </div> <!-- end tab-content -->
                </div> <!-- end card-box-->

            </div> <!-- end col -->
        </div>
        <!-- end row-->

    </div> <!-- container -->
@endsection

@section('css')

    <link href="{{ asset('backend/assets/libs/custombox/custombox.min.css')}}" rel="stylesheet">
    <link href="{{ asset('backend/assets/css/Custom-File-Upload.css')}}" rel="stylesheet">


@endsection
@section('js')

    <!-- Modal-Effect -->
    <script src="{{ asset('backend/assets/libs/custombox/custombox.min.js')}}"></script>

    <script src="{{ asset('backend/assets/js/Custom-File-Upload.js')}}"></script>

@endsection
