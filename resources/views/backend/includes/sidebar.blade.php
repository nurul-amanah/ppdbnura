<div class="left-side-menu">

    <div class="slimscroll-menu">

        <!--- Sidemenu -->
        <div id="sidebar-menu">

            <ul class="metismenu" id="side-menu">

                <li class="menu-title">Dashboard</li>

                <li>
                    <a href="javascript: void(0);">
                        <i class="fe-airplay"></i>
                        <span> Dashboards </span>
                    </a>
                </li>

                <li>
                    <a href="javascript: void(0);">
                        <i class="fe-user"></i>
                        <span> Calon Siswa </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="{{ route('santri.index', ['status' => 'Pondok']) }}">Santri</a>
                        </li>
                        <li>
                            <a href="{{ route('santri.index', ['status' => 'mts']) }}">Siswa MTs</a>
                        </li>
                        <li>
                            <a href="{{ route('santri.index', ['status' => 'smp']) }}">Siswa SMP</a>
                        </li>
                        <li>
                            <a href="{{ route('santri.index', ['status' => 'sma']) }}">Siswa SMA</a>
                        </li>
                        <li>
                            <a href="{{ route('santri.index', ['status' => 'smk']) }}">Siswa SMK</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="javascript: void(0);">
                        <i class="fe-settings"></i>
                        <span> Pengaturan </span>
                    </a>
                </li>
            </ul>

        </div>
        <!-- End Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
