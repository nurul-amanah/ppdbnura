<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Dashboard - PPDB Nurul Amanah</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ url('/') }}/admin/assets/images/favicon.ico">

        <!-- Plugins css -->
        @yield('css')

        <!-- App css -->
        <link href="{{ url('/') }}/admin/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ url('/') }}/admin/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ url('/') }}/admin/assets/css/app.min.css" rel="stylesheet" type="text/css" />

    </head>

    <body>
        @include('sweetalert::alert')


        <!-- Begin page -->
        <div id="wrapper">

            <!-- Topbar Start -->
            @include('backend.includes.navbar')
            <!-- end Topbar -->

            <!-- ========== Left Sidebar Start ========== -->
            @include('backend.includes.sidebar')
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <div class="content">

                    <!-- Start Content-->
                    @yield('content')
                    <!-- content -->
                </div>
                <!-- Footer Start -->
                @include('backend.includes.footer')
                <!-- end Footer -->

            </div>


            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->


        <!-- Right bar overlay-->

        <!-- Vendor js -->

        <script src="{{ url('/') }}/backend/assets/js/vendor.min.js"></script>

        @yield('js')
        <!-- Dashboar 1 init js-->

        <!-- App js-->
        <script src="{{ url('/') }}/backend/assets/js/app.min.js"></script>

    </body>
</html>
