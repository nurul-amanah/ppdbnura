@extends('backend.layouts.app')

@section('content')
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <form class="form-inline">
                        <div class="form-group">
                            <div class="input-group input-group-sm">

                                <input type="text" class="form-control border-white" id="datepicker">
                                <div class="input-group-append">
                                    <span class="input-group-text bg-blue border-blue text-white">
                                        <i class="mdi mdi-calendar-range font-13"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <a href="javascript: void(0);" class="btn btn-blue btn-sm ml-2">
                            <i class="mdi mdi-autorenew"></i>
                        </a>
                        <a href="javascript: void(0);" class="btn btn-blue btn-sm ml-1">
                            <i class="mdi mdi-filter-variant"></i>
                        </a>
                    </form>
                </div>
                <h4 class="page-title">Verifikasi Data Santri: <span class="text-danger">
                    {{ $santri->nama_santri }}</span></h4>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-success text-white">
                    Verifikasi Data
                </div>
                <div class="card-body">
                    <form action="{{ route('santri.verifikasi', $santri->id) }}" method="post">
                        @method('PUT')
                        @csrf
                        <table id="basic-datatable" class="table responsive nowrap table-bordered table-striped table-sm">
                            <thead>
                                <tr>
                                    <th scope="row">NO</th>
                                    <th>Nama Berkas</th>
                                    <th class="text-center">Berkas</th>
                                    <th class="text-center">Option</th>
                                </tr>
                            </thead>
                            @if (App\Models\Verifikasi::where('santri_id', $santri->id)->count() > 0)
                            <tbody>
                                <tr>
                                    <td class="align-middle">1</td>
                                    <td class="align-middle">Fotocopy Ijazah/SKHU 2 Lembar</td>
                                    <td class="align-middle text-center"><span class="badge {{ $verifikasi->fc_ijazah == 'Belum' ? 'bg-soft-danger' : 'bg-soft-success' }} {{ $verifikasi->fc_ijazah == 'Belum' ? 'text-danger' : 'text-success' }} font-15">{{ $verifikasi->fc_ijazah }}</span></td>
                                    <td class="align-middle text-center">
                                        <div class="">
                                            <input id="fc-ijazah" name="fc_ijazah" type="checkbox" data-toggle="toggle" data-on="Verification" data-off="Not Verified" data-onstyle="success" data-offstyle="danger">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">2</td>
                                    <td class="align-middle">Surat keterangan lulus</td>
                                    <td class="align-middle text-center"><span class="badge {{ $verifikasi->skl == 'Belum' ? 'bg-soft-danger' : 'bg-soft-success' }} {{ $verifikasi->skl == 'Belum' ? 'text-danger' : 'text-success' }} font-15">{{ $verifikasi->skl }}</span></td>
                                    <td class="align-middle text-center">
                                        <div class="">
                                            <input id="skl" name="skl" type="checkbox" data-toggle="toggle" data-on="Verification" data-off="Not Verified" data-onstyle="success" data-offstyle="danger">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">3</td>
                                    <td class="align-middle">Foto Diri Terbaru 3x4 (5 Lembar)</td>
                                    <td class="align-middle text-center"><span class="badge {{ $verifikasi->foto == 'Belum' ? 'bg-soft-danger' : 'bg-soft-success' }} {{ $verifikasi->foto == 'Belum' ? 'text-danger' : 'text-success' }} font-15">{{ $verifikasi->foto }}</span></td>
                                    <td class="align-middle text-center">
                                        <div class="">
                                            <input id="foto" name="foto" type="checkbox" data-toggle="toggle" data-on="Verification" data-off="Not Verified" data-onstyle="success" data-offstyle="danger">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">4</td>
                                    <td class="align-middle">Fotocopy KK 2 Lembar</td>
                                    <td class="align-middle text-center"><span class="badge {{ $verifikasi->fc_kak == 'Belum' ? 'bg-soft-danger' : 'bg-soft-success' }} {{ $verifikasi->fc_kak == 'Belum' ? 'text-danger' : 'text-success' }} font-15">{{ $verifikasi->fc_kak }}</span></td>
                                    <td class="align-middle text-center">
                                        <div class="">
                                            <input id="fc-kk" name="fc_kk" type="checkbox" data-toggle="toggle" data-on="Verification" data-off="Not Verified" data-onstyle="success" data-offstyle="danger">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">5</td>
                                    <td class="align-middle">Fotocopy Akta kelahiran 2 Lembar</td>
                                    <td class="align-middle text-center"><span class="badge {{ $verifikasi->fc_akta == 'Belum' ? 'bg-soft-danger' : 'bg-soft-success' }} {{ $verifikasi->fc_akta == 'Belum' ? 'text-danger' : 'text-success' }} font-15">{{ $verifikasi->fc_akta }}</span></td>
                                    <td class="align-middle text-center">
                                        <div class="">
                                            <input id="fc-akta" name="fc_akta" type="checkbox" data-toggle="toggle" data-on="Verification" data-off="Not Verified" data-onstyle="success" data-offstyle="danger">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">6</td>
                                    <td class="align-middle">Fotocopy KTP Orang tua 2 Lembar</td>
                                    <td class="align-middle text-center"><span class="badge {{ $verifikasi->fc_ktp_ortu == 'Belum' ? 'bg-soft-danger' : 'bg-soft-success' }} {{ $verifikasi->fc_ktp_ortu == 'Belum' ? 'text-danger' : 'text-success' }} font-15">{{ $verifikasi->fc_ktp_ortu }}</span></td>
                                    <td class="align-middle text-center">
                                        <div class="">
                                            <input id="fc-ktp-ortu" name="fc_ktp_ortu" type="checkbox" data-toggle="toggle" data-on="Verification" data-off="Not Verified" data-onstyle="success" data-offstyle="danger">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">7</td>
                                    <td class="align-middle">Fotocopy Kartu Indonesia Pintar (KIP) jika punya</td>
                                    <td class="align-middle text-center"><span class="badge {{ $verifikasi->fc_kip == 'Belum' ? 'bg-soft-danger' : 'bg-soft-success' }} {{ $verifikasi->fc_kip == 'Belum' ? 'text-danger' : 'text-success' }} font-15">{{ $verifikasi->fc_kip }}</span></td>
                                    <td class="align-middle text-center">
                                        <div class="">
                                            <input id="fc-kip" name="fc_kip" type="checkbox" data-toggle="toggle" data-on="Verification" data-off="Not Verified" data-onstyle="success" data-offstyle="danger">
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                            @else
                            <tbody>
                                <tr>
                                    <td class="align-middle">1</td>
                                    <td class="align-middle">Fotocopy Ijazah/SKHU 2 Lembar</td>
                                    <td class="align-middle text-center"><span class="badge bg-soft-danger text-danger font-15">Belum</span></td>
                                    <td class="align-middle text-center">
                                        <div class="">
                                            <input id="fc-ijazah" name="fc-ijazah" type="checkbox" data-toggle="toggle" data-on="Verification" data-off="Not Verified" data-onstyle="success" data-offstyle="danger">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">2</td>
                                    <td class="align-middle">Surat keterangan lulus</td>
                                    <td class="align-middle text-center"><span class="badge bg-soft-danger text-danger font-15">Belum</span></td>
                                    <td class="align-middle text-center">
                                        <div class="">
                                            <input id="skl" name="skl" type="checkbox" data-toggle="toggle" data-on="Verification" data-off="Not Verified" data-onstyle="success" data-offstyle="danger">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">3</td>
                                    <td class="align-middle">Foto Diri Terbaru 3x4 (5 Lembar)</td>
                                    <td class="align-middle text-center"><span class="badge bg-soft-danger text-danger font-15">Belum</span></td>
                                    <td class="align-middle text-center">
                                        <div class="">
                                            <input id="foto" name="foto" type="checkbox" data-toggle="toggle" data-on="Verification" data-off="Not Verified" data-onstyle="success" data-offstyle="danger">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">4</td>
                                    <td class="align-middle">Fotocopy KK 2 Lembar</td>
                                    <td class="align-middle text-center"><span class="badge bg-soft-danger text-danger font-15">Belum</span></td>
                                    <td class="align-middle text-center">
                                        <div class="">
                                            <input id="fc-kk" name="fc-kk" type="checkbox" data-toggle="toggle" data-on="Verification" data-off="Not Verified" data-onstyle="success" data-offstyle="danger">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">5</td>
                                    <td class="align-middle">Fotocopy Akta kelahiran 2 Lembar</td>
                                    <td class="align-middle text-center"><span class="badge bg-soft-danger text-danger font-15">Belum</span></td>
                                    <td class="align-middle text-center">
                                        <div class="">
                                            <input id="fc-akta" name="fc-akta" type="checkbox" data-toggle="toggle" data-on="Verification" data-off="Not Verified" data-onstyle="success" data-offstyle="danger">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">6</td>
                                    <td class="align-middle">Fotocopy KTP Orang tua 2 Lembar</td>
                                    <td class="align-middle text-center"><span class="badge bg-soft-danger text-danger font-15">Belum</span></td>
                                    <td class="align-middle text-center">
                                        <div class="">
                                            <input id="fc-ktp-ortu" name="fc-ktp-ortu" type="checkbox" data-toggle="toggle" data-on="Verification" data-off="Not Verified" data-onstyle="success" data-offstyle="danger">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="align-middle">7</td>
                                    <td class="align-middle">Fotocopy Kartu Indonesia Pintar (KIP) jika punya</td>
                                    <td class="align-middle text-center"><span class="badge bg-soft-danger text-danger font-15">Belum</span></td>
                                    <td class="align-middle text-center">
                                        <div class="">
                                            <input id="fc-kip" name="fc-kip" type="checkbox" data-toggle="toggle" data-on="Verification" data-off="Not Verified" data-onstyle="success" data-offstyle="danger">
                                        </div>
                                    </td>
                                </tr>
                            </tbody>

                            @endif

                        </table>
                        <div class="form-group float-lg-right mr-5">
                            <button class="btn btn-blue" type="submit">Update Verifikasi</button>
                        </div>
                    </form>
                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->
</div>
@endsection
@section('css')

<link rel="stylesheet" href="{{ url('/') }}/admin/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="{{ url('/') }}/admin/assets/css/bootstrap4-toggle.css">

@endsection

@section('js')

<script src="{{ url('/') }}/admin/assets/js/bootstrap4-toggle.js"></script>
@endsection
