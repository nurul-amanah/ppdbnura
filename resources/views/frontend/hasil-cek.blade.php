@extends('frontend.layouts.app')

@section('content')
<div class="jumbotron nura-gradient jumbotron-fluid m-0">
    <div class="container pt-5">
        <div class="row align-middle">
            <div class="col-md-5">
                <form action="{{ route('frontend.cek-data') }}" method="POST">
                    @csrf
                    <div class="card">
                        <div class="card-header bg-info text-light font-19">
                            Pencarian
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="nama">NISN</label>
                                <input value="{{ old('nisn') }}" type="text" class="form-control" id="nisn" name="nisn" placeholder="NISN">
                                <div class="text-danger">
                                    {{ $errors->first('nisn') }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nama">Nama Lengkap</label>
                                <input value="{{ old('nama') }}" type="text" class="form-control" id="nama" name="nama" placeholder="Nama Lengkap">
                                <div class="text-danger">
                                    {{ $errors->first('nama') }}
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-info btn-block" type="submit"> <i class="mdi mdi-search-web"></i> Cari Data</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header bg-info text-light font-19">
                        Hasil Pencarian
                    </div>
                    <div class="card-body">
                        @if ($santri)
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <td>Nama</td>
                                        <td>:</td>
                                        <td>{{ $santri->nama_santri }}</td>
                                    </tr>
                                    <tr>
                                        <td>NIM</td>
                                        <td>:</td>
                                        <td>{{ $santri->nisn }}</td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td>:</td>
                                        <td>Terdaftar Pada Tanggal <span class="text-danger"><b>{{ date('d-m-Y'), strtotime($santri->created_at) }}</b></span></td>
                                    </tr>
                                </tbody>
                            </table>
                        @else
                            <p>-- Anda masih belum terdaftar --</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
