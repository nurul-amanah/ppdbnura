<!-- footer start -->
<footer class="bg-dark footer">
    <div class="container-fluid">
        <div class="row mb-5">
            <div class="col-lg-4">
                <div class="pr-lg-4">
                    <div class="mb-4">
                        <span class="logo-light text-light text-bold font-24">Pondok Pesantren Nurul Amanah</span>
                        <!--<img src="{{ url('/') }}/frontend/images/logo-light.png" alt="" height="20">-->
                    </div>
                    <p class="text-white-50"><strong>Alamat :</strong> JL. Raya Tragah No. 09 Basanah Tanah Merah Bangkalan Madura</p>
                    <!--<p class="text-white-50">Ubold is a fully featured premium admin template built on top of awesome Bootstrap 4.1.3, modern web technology HTML5, CSS3 and jQuery.</p>-->
                </div>
            </div>

            <div class="col-lg-2">
                <div class="footer-list">
                    <!--<p class="text-white mb-2 footer-list-title">About</p>
                    <ul class="list-unstyled">
                        <li><a href="{{ url('/') }}/frontend/#"><i class="mdi mdi-chevron-right mr-2"></i>Home</a></li>
                        <li><a href="{{ url('/') }}/frontend/#"><i class="mdi mdi-chevron-right mr-2"></i>Features</a></li>
                        <li><a href="{{ url('/') }}/frontend/#"><i class="mdi mdi-chevron-right mr-2"></i>Faq</a></li>
                        <li><a href="{{ url('/') }}/frontend/#"><i class="mdi mdi-chevron-right mr-2"></i>Clients</a></li>
                    </ul>-->
                </div>
            </div>

            <div class="col-lg-2">
                <div class="footer-list">
                    <!--<p class="text-white mb-2 footer-list-title">Social</p>
                    <ul class="list-unstyled">
                        <li><a href="{{ url('/') }}/frontend/#"><i class="mdi mdi-chevron-right mr-2"></i>Facebook </a></li>
                        <li><a href="{{ url('/') }}/frontend/#"><i class="mdi mdi-chevron-right mr-2"></i>Twitter</a></li>
                        <li><a href="{{ url('/') }}/frontend/#"><i class="mdi mdi-chevron-right mr-2"></i>Behance</a></li>
                        <li><a href="{{ url('/') }}/frontend/#"><i class="mdi mdi-chevron-right mr-2"></i>Dribble</a></li>
                    </ul>-->
                </div>
            </div>

            <div class="col-lg-2">
                <div class="footer-list">
                    <!--<p class="text-white mb-2 footer-list-title">Support</p>
                    <ul class="list-unstyled">
                        <li><a href="{{ url('/') }}/frontend/#"><i class="mdi mdi-chevron-right mr-2"></i>Help & Support</a></li>
                        <li><a href="{{ url('/') }}/frontend/#"><i class="mdi mdi-chevron-right mr-2"></i>Privacy Policy</a></li>
                        <li><a href="{{ url('/') }}/frontend/#"><i class="mdi mdi-chevron-right mr-2"></i>Terms & Conditions</a></li>
                    </ul>-->
                </div>
            </div>

            <div class="col-lg-2">
                <div class="footer-list">
                    <!--<p class="text-white mb-2 footer-list-title">More Info</p>
                    <ul class="list-unstyled">
                        <li><a href="{{ url('/') }}/frontend/#"><i class="mdi mdi-chevron-right mr-2"></i>Pricing</a></li>
                        <li><a href="{{ url('/') }}/frontend/#"><i class="mdi mdi-chevron-right mr-2"></i>For Marketing</a></li>
                        <li><a href="{{ url('/') }}/frontend/#"><i class="mdi mdi-chevron-right mr-2"></i>For Agencies</a></li>
                        <li><a href="{{ url('/') }}/frontend/#"><i class="mdi mdi-chevron-right mr-2"></i>Our Apps</a></li>
                    </ul>-->
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="float-left pull-none">
                    <p class="text-white-50">2019 © NURA. Design by <a href="#" target="_blank" class="text-white-50">Santri Nura</a> </p>
                </div>
                <div class="float-right pull-none">
                    <ul class="list-inline social-links">
                        <li class="list-inline-item text-white-50">
                            Social :
                        </li>
                        <li class="list-inline-item"><a href="{{ url('/') }}/frontend/#"><i class="mdi mdi-facebook"></i></a></li>
                        <li class="list-inline-item"><a href="{{ url('/') }}/frontend/#"><i class="mdi mdi-twitter"></i></a></li>
                        <li class="list-inline-item"><a href="{{ url('/') }}/frontend/#"><i class="mdi mdi-instagram"></i></a></li>
                        <li class="list-inline-item"><a href="{{ url('/') }}/frontend/#"><i class="mdi mdi-google-plus"></i></a></li>
                    </ul>
                </div>
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- container-fluid -->
</footer>
<!-- footer end -->
