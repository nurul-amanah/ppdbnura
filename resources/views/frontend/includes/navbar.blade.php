<!--Navbar Start-->
<nav class="navbar navbar-expand-lg fixed-top navbar-custom sticky sticky-dark">
    <div class="container-fluid">
        <!-- LOGO -->
        <a class="logo text-uppercase" href="#">
            <img src="{{ url('/') }}/frontend/images/logo-02.png" alt="" class="logo-light" height="25" />
            <img src="{{ url('/') }}/frontend/images/logo-01.png" alt="" class="logo-dark" height="25" />
            <!--<span class="logo-light text-light text-bold font-24">PP. NURA</span>
            <span class="logo-dark text-black-50 text-bold font-24">PP. NURA</span>-->
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <i class="mdi mdi-menu"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mx-auto navbar-right" id="mySidenav">

            </ul>
            <a href="{{ route('frontend.alur-verifikasi') }}" class="btn btn-danger navbar-btn m-1">Alur Verifikasi</a>
            <button class="btn btn-warning navbar-btn  m-1">Cek Validasi</button>
            <a href="{{ route('santri.store') }}" class="btn btn-info navbar-btn m-1">Daftar</a>
        </div>
    </div>
</nav>
<!-- Navbar End -->
