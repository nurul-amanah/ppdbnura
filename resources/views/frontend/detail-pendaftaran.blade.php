@extends('frontend.layouts.app')

@section('content')
{{-- {{ dd(['nama_santri']) }} --}}
<div class="container mt-5" >
    <div class="card shadow-sm" style="margin-top: 170px; margin-bottom: 70px;">
        <div class="card-header bg-blue text-light text-center">
            Biodata Santri
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-borderless">
                    <tr>
                        <th scope="row">NISN</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['nisn'] }}</td>
                        <td rowspan="4"> <img src="" width="100px" alt=""> </td>
                    </tr>
                    <tr>
                        <th scope="row">NIK</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['npsn'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Nama Lengkap</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['nama_santri'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Nama Panggilan</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['panggilan_santri'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Jenis Kelamin</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['jenis_kelamin'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Tempat Lahir</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['tempat_lahir'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Tanggal Lahir</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['tanggal_lahir'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Anak Ke</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['anak_ke'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Jumlah Saudara</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['jumlah_saudara'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Status Dalam Keluarga</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['status_dalam_keluarga'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Alamat</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['alamat'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Masuk Jenjang Pendidikan</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['jenjang_pendidikan'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Satus Siswa</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['status_siswa'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Asal Sekolah</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['asal_sekolah'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Alamat Sekolah</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['alamat_sekolah'] }}</td>
                    </tr>

                    <tr>
                        <th class="text-center bg-info font-18 text-light" colspan="4" >Informasi Ayah</th>
                    </tr>

                    <tr>
                        <th scope="row">Nama Ayah</th>
                        <td>:</td>
                        <td>{{ Session::get('ayahSiswa')['nama_ayah'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Agama Ayah</th>
                        <td>:</td>
                        <td>{{ Session::get('ayahSiswa')['agama_ayah'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Pendidikan Trakhir Ayah</th>
                        <td>:</td>
                        <td>{{ Session::get('ayahSiswa')['pendidikan_terakhir_ayah'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Pekerjaan Ayah</th>
                        <td>:</td>
                        <td>{{ Session::get('ayahSiswa')['pekerjaan_ayah'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Alamat Ayah</th>
                        <td>:</td>
                        <td>{{ Session::get('ayahSiswa')['alamat_ayah'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Penghasilan Orang Tua Perbulan</th>
                        <td>:</td>
                        <td>Rp. {{ Session::get('ayahSiswa')['penghasilan_ayah'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Nomor HP/WA Orang Tua/Wali</th>
                        <td>:</td>
                        <td>{{ Session::get('ayahSiswa')['nomor_hp'] }}</td>
                    </tr>
                    <tr>
                        <th class="text-center bg-info font-18 text-light" colspan="4" >Informasi Ibu</th>
                    </tr>
                    <tr>
                        <th scope="row">Nama Ibu</th>
                        <td>:</td>
                        <td>{{ Session::get('ibuSiswa')['nama_ibu'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Agama Ibu</th>
                        <td>:</td>
                        <td>{{ Session::get('ibuSiswa')['agama_ibu'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Pendidikan Trakhir Ibu</th>
                        <td>:</td>
                        <td>{{ Session::get('ibuSiswa')['pendidikan_terakhir_ibu'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Pekerjaan Ibu</th>
                        <td>:</td>
                        <td>{{ Session::get('ibuSiswa')['pekerjaan_ibu'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Alamat Ibu</th>
                        <td>:</td>
                        <td>{{ Session::get('ibuSiswa')['alamat_ibu'] }}</td>
                    </tr>
                </table>
            </div>
            <!--<div class="table-responsive">
                <table class="table table-sm table-borderless">
                    <tr class="font-20 bg-info text-light">
                        <th scope="row">Rincian Pembiayaan</th>
                        <td> </td>
                        <td>Biaya</td>
                    </tr>
                    <tr>
                        <th scope="row">Iuran Osis 1 Tahun</th>
                        <td>:</td>
                        <td>Rp. 50.000</td>
                    </tr>
                    <tr>
                        <th scope="row">Map Rapot</th>
                        <td>:</td>
                        <td>Rp. 60.000</td>
                    </tr>
                    <tr>
                        <th scope="row">Kegiatan MPLS</th>
                        <td>:</td>
                        <td>Rp. 35.000</td>
                    </tr>
                    <tr>
                        <th scope="row">Kegiatan Agustusan</th>
                        <td>:</td>
                        <td>Rp. 20.000</td>
                    </tr>
                    <tr>
                        <th scope="row">SPP Bulan Juli 2020</th>
                        <td>:</td>
                        <td>Rp. 100.000</td>
                    </tr>
                    <tr>
                        <th scope="row">Tabungan Bulan Juli</th>
                        <td>:</td>
                        <td>Rp. 20.000</td>
                    </tr>
                    <tr>
                        <th scope="row">Dana Kesehatan</th>
                        <td>:</td>
                        <td>Rp. 20.000</td>
                    </tr>
                    <tr>
                        <th scope="row">Infaq Pendidikan Juli</th>
                        <td>:</td>
                        <td>Rp. 10.000</td>
                    </tr>
                    <tr>
                        <th scope="row">Kartu Pelajar Digital</th>
                        <td>:</td>
                        <td>Rp. 50.000</td>
                    </tr>
                    <tr>
                        <th scope="row">Kain Seragam Batik 1 Stel</th>
                        <td>:</td>
                        <td>Rp. 150.000</td>
                    </tr>
                    <tr>
                        <th scope="row">Kain Seragam Khas 1 Stel</th>
                        <td>:</td>
                        <td>Rp. 150.000</td>
                    </tr>
                    <tr>
                        <th scope="row">Kaos Olah Raga</th>
                        <td>:</td>
                        <td>Rp. 125.000</td>
                    </tr>
                    <tr>
                        <th scope="row">Wear Pack / Seragam Lab</th>
                        <td>:</td>
                        <td>Rp. 250.000</td>
                    </tr>
                    <tr>
                        <th scope="row">Kopyah/Hijab @ 65.000</th>
                        <td>:</td>
                        <td>Rp. 130.000</td>
                    </tr>
                    <tr>
                        <th scope="row">Dasi @ 20.000</th>
                        <td>:</td>
                        <td>Rp. 40.000</td>
                    </tr>
                    <tr>
                        <th scope="row">Atribut Sekolah</th>
                        <td>:</td>
                        <td>Rp. 50.000</td>
                    </tr>
                    <tr>
                        <th scope="row">Ongkos Jahit @ 50.000</th>
                        <td>:</td>
                        <td>Rp. 100.000</td>
                    </tr>
                    <tr class="font-20">
                        <th scope="row">Jumlah</th>
                        <td>:</td>
                        <td>Rp. 1.xxx.xxx</td>
                    </tr>
                    <tr>
                        <th class="text-center bg-info font-18 text-light" colspan="4" >Biaya Bagi Yang Mundok</th>
                    </tr>
                    <tr>
                        <th scope="row">Pendaftaran Pondok</th>
                        <td>:</td>
                        <td>Rp. 400.000</td>
                    </tr>
                    <tr>
                        <th scope="row">Syahriyah Pondok</th>
                        <td>:</td>
                        <td>Rp. 350.000</td>
                    </tr>
                    <tr>
                        <th scope="row">Buku Wajib Santri</th>
                        <td>:</td>
                        <td>Rp. 50.000</td>
                    </tr>
                    <tr>
                        <th scope="row">Jas Almamater Pondok</th>
                        <td>:</td>
                        <td>Rp. 150.000</td>
                    </tr>
                    <tr>
                        <th scope="row">Seragam Madrasah</th>
                        <td>:</td>
                        <td>Rp. 400.000</td>
                    </tr>
                    <tr>
                        <th scope="row">Madrasah Diniyah 1 Tahun</th>
                        <td>:</td>
                        <td>Rp. 150.000</td>
                    </tr>
                    <tr class="font-20">
                        <th scope="row">Total Biaya Pondok</th>
                        <td>:</td>
                        <td>Rp. 1.500.000</td>
                    </tr>
                    <tr>
                        <th class=" bg-danger text-light font-24 text-center" colspan="4">TOTAL Yang Harus Di BAYAR : RP. 2.xxx.xxx </th>
                    </tr>
                </table>
                <p class="badge bg-soft-warning text-warning font-15 p-2">
                    NB: Pembayaran bisa ditransfer di Rekening BNI SYARIAH atas Nama Siti Mutiah, 0987666557
                </p>
            </div>-->
        </div>
    </div>
</div>

@endsection
