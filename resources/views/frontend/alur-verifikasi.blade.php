@extends('frontend.layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mt-5">
        <div class="col-md-8">
            <table id="basic-datatable" class="table responsive nowrap table-bordered table-striped table-sm">
                <thead>
                    <tr>
                        <th style="width: 20px;" >No</th>
                        <th>Nama Berkas</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Bukti Pembayaran</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Fotocopy Ijazah/SKHU 2 Lembar</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Surat keterangan lulus</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Foto Diri Terbaru 3x4 (5 Lembar)</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Fotocopy KK 2 Lembar</td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>Fotocopy Akta kelahiran 2 Lembar</td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>Fotocopy KTP Orang tua 2 Lembar</td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>Fotocopy Kartu Indonesia Pintar (KIP) jika punya.</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('css')

<link rel="stylesheet" href="{{ url('/') }}/admin/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="{{ url('/') }}/admin/assets/css/bootstrap4-toggle.css">

@endsection

@section('js')

<script src="{{ url('/') }}/admin/assets/js/bootstrap4-toggle.js"></script>
@endsection
