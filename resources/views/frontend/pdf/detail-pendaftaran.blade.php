<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
    <div class="card shadow-sm" style="margin-top: 170px; margin-bottom: 70px;">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-borderless">
                    <tr>
                        <th scope="row">NISN</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['nisn'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">NPSN</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['npsn'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Nama Lengkap</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['nama_santri'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Nama Panggilan</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['panggilan_santri'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Jenis Kelamin</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['jenis_kelamin'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Tempat Lahir</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['tempat_lahir'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Tanggal Lahir</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['tanggal_lahir'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Anak Ke</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['anak_ke'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Jumlah Saudara</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['jumlah_saudara'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Status Dalam Keluarga</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['status_dalam_keluarga'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Alamat</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['alamat'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Masuk Jenjang Pendidikan</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['jenjang_pendidikan'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Satus Siswa</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['status_siswa'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Asal Sekolah</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['asal_sekolah'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Alamat Sekolah</th>
                        <td>:</td>
                        <td>{{ Session::get('siswaBaru')['alamat_sekolah'] }}</td>
                    </tr>

                </table>
            </div>
        </div>

        DATA ORANG AYAH
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-borderless">
                    <tr>
                        <th scope="row">Nama Ayah</th>
                        <td>:</td>
                        <td>{{ Session::get('ayahSiswa')['nama_ayah'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Agama Ayah</th>
                        <td>:</td>
                        <td>{{ Session::get('ayahSiswa')['agama_ayah'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Pendidikan Trakhir Ayah</th>
                        <td>:</td>
                        <td>{{ Session::get('ayahSiswa')['pendidikan_terakhir_ayah'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Pekerjaan Ayah</th>
                        <td>:</td>
                        <td>{{ Session::get('ayahSiswa')['pekerjaan_ayah'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Alamat Ayah</th>
                        <td>:</td>
                        <td>{{ Session::get('ayahSiswa')['alamat_ayah'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Penghasilan Orang Tua Perbulan</th>
                        <td>:</td>
                        <td>Rp. {{ Session::get('ayahSiswa')['penghasilan_ayah'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Nomor HP/WA Orang Tua/Wali</th>
                        <td>:</td>
                        <td>{{ Session::get('ayahSiswa')['nomor_hp'] }}</td>
                    </tr>
                </table>
            </div>
        </div>
        DATA IBU
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-borderless">
                    <tr>
                        <th scope="row">Nama Ibu</th>
                        <td>:</td>
                        <td>{{ Session::get('ibuSiswa')['nama_ibu'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Agama Ibu</th>
                        <td>:</td>
                        <td>{{ Session::get('ibuSiswa')['agama_ibu'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Pendidikan Trakhir Ibu</th>
                        <td>:</td>
                        <td>{{ Session::get('ibuSiswa')['pendidikan_terakhir_ibu'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Pekerjaan Ibu</th>
                        <td>:</td>
                        <td>{{ Session::get('ibuSiswa')['pekerjaan_ibu'] }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Alamat Ibu</th>
                        <td>:</td>
                        <td>{{ Session::get('ibuSiswa')['alamat_ibu'] }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>
