@extends('frontend.layouts.app')

@section('content')
<div class="jumbotron nura-image jumbotron-fluid">
    <div class="container p-5">
        <div class="row align-middle">
            <div class="col-md-8">
                <h1 class="display-4 text-white">Ponpes Nurul Amanah</h1>
                <p class="lead text-white">Selamat datang di Pendaftaran Online Pesantren Nurul Amanah</p>
            </div>
            <div class="col-md-4">
                <img src="{{ asset('frontend/images/ayomondok-icon.png')}}" class="img-fluid float-right" width="100%;" alt="">
            </div>
        </div>
    </div>
</div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-11">
                <div class="alert alert-info">
                    Selamat datang di Pendaftaran Online Ponpen Nurul Amanah, untuk pendaftaran silahkan klik pada tombol <b>Daftar</b> dan ikuti prosedur pendaftaran yang ada dibawah ini
                </div>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-lg-11">
                <div class="card-box">
                    <h4 class="header-title"> Panduan Daftar </h4>
                    <!--<p class="sub-header">Use class <code>.embed-responsive-16by9</code></p>-->
                    <!-- 16:9 aspect ratio -->
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="{{ url('/') }}/frontend/video/panduan-pendaftaran.mp4"></iframe>
                    </div>
                </div>
            </div> <!-- end col -->
            <div class="col-md-11">
                <div class="">
                    <div class="container">
                        <h4>Timeline</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="main-timeline4">
                                    <div class="timeline">
                                        <divs class="timeline-content">
                                            <span class="year">1</span>
                                            <div class="inner-content">
                                                <h3 class="title">MENGISI FORMULIR</h3>
                                                <p class="description">
                                                    Pengisian bisa dilakukan secara Online di website <strong><a href="https://ppdb.nurulamanah.or.id/pendaftaran-santri-baru" target="_blank">PPDB NURA</a></strong> atau dengan datang langsung di kantor Yayasan Pondok Pesantren Nurul Amanah.
                                                </p>
                                            </div>
                                        </divs>
                                    </div>
                                    <div class="timeline">
                                        <div class="timeline-content">
                                            <span class="year">2</span>
                                            <div class="inner-content">
                                                <h3 class="title">PEMBAYARAN</h3>
                                                <p class="description">
                                                    Silahkan melakukan pembayaran melalui transfer via Bank atau datang langsung ke kantor Yayasan Pondok Pesantren Nurul Amanah.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="timeline">
                                        <a href="#" class="timeline-content">
                                            <span class="year">3</span>
                                            <div class="inner-content">
                                                <h3 class="title">VERIFIKASI</h3>
                                                <p class="description">
                                                    Silahkan membawa berkas persyaratan pendaftaran sesuai yang tertera pada halaman setelah melakukan pendaftaran.
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="timeline">
                                        <div class="timeline-content">
                                            <span class="year">4</span>
                                            <div class="inner-content">
                                                <h3 class="title">PENGUMUMAN</h3>
                                                <p class="description mb-5">
                                                    Pengumuman penerimaan akan dikirim ke nomor yang didaftarkan.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-11 text-center">
                <h4 class="header-title"> Detail Biaya Pendaftaran </h4>
                <img class="img-fluid" src="{{ url('/') }}/frontend/images/biaya.jpeg" width="700" alt="Responsive image">
                <div class="alert alert-warning mt-3">
                    NB: Pembayaran bisa ditransfer di Rekening BNI SYARIAH atas Nama Siti Mutiah, 0987666557
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')

    <link href="{{asset('frontend/css/timeline.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('js')
<script src="{{ asset('frontend/js/jquery-1.11.1.min.js')}}"></script>
@endsection

