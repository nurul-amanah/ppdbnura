@extends('frontend.layouts.app')

@section('content')
<div class="jumbotron nura-gradient jumbotron-fluid m-0">
    <div class="container pt-5">
        <div class="row align-middle">
            <div class="col-md-5">
                <form action="{{ route('frontend.cek-data') }}" method="POST">
                    @csrf
                    <div class="card">
                        <div class="card-header bg-info text-light font-19">
                            Pencarian
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="nama">NISN</label>
                                <input value="{{ old('nisn') }}" type="text" class="form-control" id="nisn" name="nisn" placeholder="NISN">
                                <div class="text-danger">
                                    {{ $errors->first('nisn') }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nama">Nama Lengkap</label>
                                <input value="{{ old('nama') }}" type="text" class="form-control" id="nama" name="nama" placeholder="Nama Lengkap">
                                <div class="text-danger">
                                    {{ $errors->first('nama') }}
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-info btn-block" type="submit"> <i class="mdi mdi-search-web"></i> Cari Data</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header bg-info text-light font-19">
                        Hasil Pencarian
                    </div>
                    <div class="card-body">
                        <p class="text-center">-- Data Kosong --</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
