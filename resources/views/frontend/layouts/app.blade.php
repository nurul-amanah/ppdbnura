<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>PPDB - Nurul Amanah</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Sistem Informasi pendaftaran santri baru Pondok Pesantren Nurul Amanah Berbasis Web" name="description" />
        <meta content="Santri Nurul Amanah" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        @yield('css')

        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ url('/') }}/frontend/images/icon.ico">

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="{{ url('/') }}/frontend/css/bootstrap.min.css" type="text/css">

        <!--Material Icon -->
        <link rel="stylesheet" type="text/css" href="{{ url('/') }}/frontend/css/materialdesignicons.min.css" />

        <!-- Custom  sCss -->
        <link rel="stylesheet" type="text/css" href="{{ url('/') }}/frontend/css/style.css" />

    </head>

    <body>
        @include('sweetalert::alert')

        @include('frontend.includes.navbar')

        @yield('content')

        @include('frontend.includes.footer')

        <!-- Back to top -->
        <a href="{{ url('/') }}/frontend/#" class="back-to-top" id="back-to-top"> <i class="mdi mdi-chevron-up"> </i> </a>

        @yield('js')
        <!-- javascript -->
        <script src="{{ url('/') }}/frontend/js/jquery.min.js"></script>
        <script src="{{ url('/') }}/frontend/js/bootstrap.bundle.min.js"></script>
        <script src="{{ url('/') }}/frontend/js/jquery.easing.min.js"></script>
        <script src="{{ url('/') }}/frontend/js/scrollspy.min.js"></script>
        <script src="//code.jivosite.com/widget/CsbscFzcH6" async></script>
        <!-- custom js -->
        <script src="{{ url('/') }}/frontend/js/app.js"></script>
    </body>

</html>
