@extends('frontend.layouts.app')

@section('content')
<!-- home start -->
<section class="bg-home bg-gradient" id="home">
    <div class="home-center">
        <div class="home-desc-center">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="home-title mo-mb-20">
                            <h1 class="mb-4 text-white">Ubold is a fully featured premium admin template</h1>
                            <p class="text-white-50 home-desc mb-5">Ubold is a fully featured premium admin template built on top of awesome Bootstrap 4.1.3, modern web technology HTML5, CSS3 and jQuery. It has many ready to use hand crafted components. </p>
                            <div class="subscribe">
                                <form>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="mb-2">
                                                <input type="text" class="form-control" placeholder="Enter your e-mail address">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <button type="submit" class="btn btn-primary">Subscribe Us</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 offset-xl-2 col-lg-5 offset-lg-1 col-md-7">
                        <div class="home-img position-relative">
                            <img src="{{ url('/') }}/frontend/images/home-img.png" alt="" class="home-first-img">
                            <img src="{{ url('/') }}/frontend/images/home-img.png" alt="" class="home-second-img mx-auto d-block">
                            <img src="{{ url('/') }}/frontend/images/home-img.png" alt="" class="home-third-img">
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div>
            <!-- end container-fluid -->
        </div>
    </div>
</section>
<!-- home end -->

<div class="col-sm-12">
    <div class="row bs-wizard" style="border-bottom:0;">
        <div class="col-xs-2 col-xs-offset-1 bs-wizard-step active">
          <div class="progress"><div class="progress-bar"></div></div>
          <span class="bs-wizard-dot">
              <i class="fa fa-pencil-square-o"></i>
          </span>
          <div class="text-center bs-wizard-stepnum">Tulis Laporan</div>
          <div class="bs-wizard-info text-center">
              Laporkan keluhan atau aspirasi anda dengan jelas dan lengkap
          </div>
        </div>

        <div class="col-xs-2 bs-wizard-step">
          <div class="progress"><div class="progress-bar"></div></div>
          <span class="bs-wizard-dot">
              <i class="fa fa-mail-forward"></i>
          </span>
          <div class="text-center bs-wizard-stepnum">Proses Verifikasi</div>
          <div class="bs-wizard-info text-center">
              Dalam 3 hari, laporan Anda akan diverifikasi dan diteruskan kepada instansi berwenang
          </div>
        </div>

        <div class="col-xs-2 bs-wizard-step">
          <div class="progress"><div class="progress-bar"></div></div>
          <span class="bs-wizard-dot">
              <i class="fa fa-comments"></i>
          </span>
          <div class="text-center bs-wizard-stepnum">Proses Tindak Lanjut</div>
          <div class="bs-wizard-info text-center">
              Dalam 5 hari, instansi akan menindaklanjuti dan membalas laporan Anda
          </div>
        </div>

        <div class="col-xs-2 bs-wizard-step">
          <div class="progress"><div class="progress-bar"></div></div>
          <span class="bs-wizard-dot">
              <i class="fa fa-commenting-o"></i>
          </span>
          <div class="text-center bs-wizard-stepnum">Beri Tanggapan</div>
          <div class="bs-wizard-info text-center">
              Anda dapat menanggapi kembali balasan yang diberikan oleh instansi dalam waktu 10 hari
          </div>
        </div>

        <div class="col-xs-2 bs-wizard-step">
          <div class="progress"><div class="progress-bar"></div></div>
          <span class="bs-wizard-dot">
              <i class="fa fa-check"></i>
          </span>
          <div class="text-center bs-wizard-stepnum">Selesai</div>
          <div class="bs-wizard-info text-center">
              Laporan Anda akan terus ditindaklanjuti hingga terselesaikan
          </div>
        </div>

    </div>
</div>
@endsection
